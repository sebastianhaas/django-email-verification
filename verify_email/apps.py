import logging

from django.apps import AppConfig
from django.core.exceptions import ImproperlyConfigured

logger = logging.getLogger(__name__)


class VerifyEmailConfig(AppConfig):
    name = 'verify_email'
    default_auto_field = 'django.db.models.BigAutoField'

    def ready(self):
        from django.conf import settings
        if 'mail_templated' not in settings.INSTALLED_APPS:
            raise ImproperlyConfigured('mail_templated must be in installed apps.')

        logger.info('[Email Verification] : importing signals    - OK.')
        import verify_email.signals
