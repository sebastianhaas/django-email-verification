from mail_templated import send_mail

from .app_configurations import GetFieldFromSettings
from .errors import InvalidTokenOrEmail
from .token_manager import TokenManager


class _VerifyEmail:
    """
    This class does two things:
    1. set each user as inactive and saves it
    2. sends the email to user with that link.
    """

    def __init__(self):
        self.settings = GetFieldFromSettings()
        self.token_manager = TokenManager()

    # Private :
    def __send_email(self, template_name: str, context: dict, user_email_address: str):
        send_mail(
            template_name=template_name,
            context=context,
            from_email=self.settings.get('from_alias'),
            recipient_list=[user_email_address],
        )

    # Public :
    def send_verification_link(self, request, inactive_user=None, form=None):
        if form:
            inactive_user = form.save(commit=False)
        inactive_user.is_active = False
        inactive_user.save()

        try:
            if form:
                user_email_address = form.cleaned_data.get(self.settings.get('email_field_name'))
                if not user_email_address:
                    raise KeyError(
                        'No key named "email" in your form. Your field should be named as email in form OR set a '
                        'variable "EMAIL_FIELD_NAME" with the name of current field in settings.py if you want to use '
                        'current name as email field.'
                    )
            else:
                user_email_address = inactive_user.email

            verification_url = self.token_manager.generate_link(request, inactive_user, user_email_address)
            self.__send_email(
                template_name=self.settings.get('email_template_send_link', raise_exception=True),
                context={"link": verification_url, "inactive_user": inactive_user},
                user_email_address=user_email_address,
            )

            return inactive_user
        except Exception:
            inactive_user.delete()
            raise

    def resend_verification_link(self, request, user_email_address, **kwargs):
        """
        This method needs the previously sent link to get encoded email and token from that.
        Exceptions Raised
        -----------------
            - UserAlreadyActive (by) get_user_by_token()
            - MaxRetryExceeded  (by) request_new_link()
            - InvalidTokenOrEmail
        
        These exception should be handled in caller function.
        """
        inactive_user = kwargs.get('user')
        user_encoded_token = kwargs.get('token')
        encoded = kwargs.get('encoded', True)

        if encoded:
            decoded_encrypted_user_token = self.token_manager.perform_decoding(user_encoded_token)
            user_email_address = self.token_manager.perform_decoding(user_email_address)
            inactive_user = self.token_manager.get_user_by_token(user_email_address, decoded_encrypted_user_token)

        if not inactive_user or not user_email_address:
            raise InvalidTokenOrEmail(f'Either token or email is invalid. user: {inactive_user}, email: {user_email_address}')

        # At this point, we have decoded email(if it was encoded), and inactive_user, and we can request new link
        verification_url = self.token_manager.request_new_link(request, inactive_user, user_email_address)

        self.__send_email(
            template_name=self.settings.get('email_template_resend_link', raise_exception=True),
            context={"link": verification_url, "inactive_user": inactive_user},
            user_email_address=user_email_address
        )
        return True


#  This is supposed to be called outside of this module
def send_verification_email(request, form):
    return _VerifyEmail().send_verification_link(request, form=form)


#  This is supposed to be called outside of this module
def resend_verification_email(request, email, **kwargs):
    return _VerifyEmail().resend_verification_link(request, email, **kwargs)
